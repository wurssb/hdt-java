#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Pull latest
git -C "$DIR" pull

mvn install -f "$DIR/pom.xml"
# gradle install -b "$DIR/build.gradle" 
#--quiet

#mv $DIR/target/*-with-dependencies*jar $DIR/NG-Tax.jar
